package edu.kau.fcit.cpit252;

import edu.kau.fcit.cpit252.geoLocation.GeoLocation;
import edu.kau.fcit.cpit252.weatherDB.WeatherDBI;

public class App {
    public static void main(String[] args) {
        // get weather for Jeddah => 21.543333, 39.172778

        WeatherCity wCity = new WeatherDBI();
        WeatherGeo weatherAdapter = new WeatherAdapter(wCity);
        String weatherInfo = weatherAdapter.getWeatherInfo(21.543333, 39.172778);
        System.out.println(weatherInfo);
    }
}
