Hassan Asim Tayeb
1936820

Q2 Answer:
Using an interface instead of a concrete class will make the code easy to change. Instead of creating the adapter for a specific API and changing the adapter for every single API, the adapter will work for all other APIs with only changing the name of the invoked API in the App class.
